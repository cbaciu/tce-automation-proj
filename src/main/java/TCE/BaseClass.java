/**
 * 
 */
package TCE;

import junit.framework.TestCase;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
/**
 * @author cbaciu
 * 
 */
public class BaseClass extends TestCase {

	WebDriver driver;
	private static String NodeURL = "http://localhost:4445/wd/hub";
	private String browserName;
	public String bt;
	public String baseURL;
	private Process p1,p2,p3;
	DesiredCapabilities capab;

	public String getBrowsername() {
		return browserName;
	}

	public void setBrowsername(String browsername) {
		this.browserName = browsername;
	}
	
    public String getBaseURL() {
		return baseURL;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public void BeforeAll() {

	}

	public void BeforeEach() {

	}
	@Before
	public void setUp() throws Exception {
		//super.setUp(); - not needed

		BeforeEach();
		System.out.println("   ### After before each, the value of browserName: " + browserName + " #");
		//System.setProperty("browserJenkins","chrome");

		String bws = System.getProperty("browserJenkinsLocal");
		System.out.println("   ### The value of browserJenkins in base class: " + System.getProperty("browserJenkins") + " #");
		
/*		if ((browserJenkins != null) || "".equals(browserJenkins))
			browserName = browserJenkins;
		else {
			browserName = "chrome";//chrome
		}*/
		System.out.println("Checkpoint - before the switch - browserName = " + browserName);
		//switch for browser to run
		switch (browserName) { //before -> switch (browserJenkins) {
		
		case "chrome":
			System.setProperty("webdriver.chrome.driver", "./src/main/java/resources/chromedriver.exe");
			capab = DesiredCapabilities.chrome();
			capab.setBrowserName(browserName);
			capab.setVersion("ANY");
			capab.setCapability("platform", Platform.ANY);
			//driver = new ChromeDriver(capab);
			driver = new RemoteWebDriver(new URL(NodeURL), capab);
			break;
		
		case "firefox":
			
			System.setProperty("webdriver.gecko.driver","./src/main/java/resources/geckodriver.exe");
			System.setProperty("webdriver.firefox.bin","c:\\Program Files\\Mozilla Firefox\\firefox.exe");
			//System.setProperty("webdriver.firefox.marionette","false");
			
			capab = DesiredCapabilities.firefox();
			//capab.setCapability("binary", "c:\\Program Files\\Mozilla Firefox\\firefox.exe");
			capab.setBrowserName(browserName);
			capab.setVersion("ANY");
			capab.setCapability("platform", Platform.ANY);
			System.out.println("   ### Firefox driver will start....");
			//driver = new FirefoxDriver(capab); // old fashion
			driver = new RemoteWebDriver(new URL(NodeURL), capab);
			
			
			break;
		
		case "internet explorer":
			System.setProperty("webdriver.ie.driver", "./src/main/java/resources/IEDriverServer x64.exe");
			capab = DesiredCapabilities.internetExplorer();
			capab.setBrowserName(browserName);
			capab.setVersion("ANY");
			capab.setCapability("platform", Platform.ANY);
			//driver = new InternetExplorerDriver(capab);
			driver = new RemoteWebDriver(new URL(NodeURL), capab);
			break;
		
		case "grid":
			capab = new DesiredCapabilities();
			capab.setBrowserName(bt);
			capab.setVersion("ANY");
			capab.setCapability("platform", Platform.ANY);
			//driver = new RemoteWebDriver(new URL(NodeURL), capab);
			driver = new RemoteWebDriver(new URL(NodeURL), capab);
			break;
		
		default:
			capab = DesiredCapabilities.chrome();
			capab.setBrowserName("chrome");
			capab.setVersion("ANY");
			capab.setCapability("platform", Platform.ANY);
			driver = new RemoteWebDriver(new URL(NodeURL), capab);
			break;
		}

		//Option 1
/*		try {
			List cmdAndArgs = Arrays.asList("cmd", "/c", "HubStart_4444.bat");
			File dir = new File(".\\src\\main\\java\\resources\\");

			ProcessBuilder pb = new ProcessBuilder(cmdAndArgs);
			pb.directory(dir);
			Process p = pb.start();
		} catch (Exception e) {
			// TODO: handle exception
		}*/

		
		//Option 2
/*		if (browserName.equals("grid")) {
			try {
	            String[] command = {"cmd.exe","/K start /d .\\src\\main\\java\\resources\\ HubStart_4444.bat"};
	            p1 =  Runtime.getRuntime().exec(command);
	            String[] command2 = {"cmd.exe","/K start /d .\\src\\main\\java\\resources\\ Node_Chrome_5555.bat"};
	            p2 =  Runtime.getRuntime().exec(command2);
	            String[] command3 = {"cmd.exe","/K start /d .\\src\\main\\java\\resources\\ Node_Firefox_5560.bat"};
	            p3 =  Runtime.getRuntime().exec(command3);
	            Thread.sleep(5000); // for the Hub and Node to start
	        } catch (IOException ex) {
	        }
	
		}*/


		//driver = new ChromeDriver(capab);
	}


	@After
    public void tearDown() throws Exception {
		driver.quit();
		bt="alfa";
		//Runtime.getRuntime().exec("taskkill /F /IM " + "TBC - pid name");

	
	}
}
