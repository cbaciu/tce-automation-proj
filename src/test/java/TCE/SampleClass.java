/**
 * 
 */
package TCE;

import java.io.File;

import org.junit.Test;

/**
 * @author cbaciu
 *
 */
public class SampleClass {

    @Test
    public void testPathConversion() {
        File f = new File("test/../test.txt");
        File f2 = new File("c:/TempCip/ParalelRunning/src/main/java/resources/geckodriver.exe");
        try {
            f.createNewFile();
            System.out.println(f.getPath());
            System.out.println(f.getAbsolutePath());    
            System.out.println(f.getCanonicalPath());
            System.out.println("   ### " + f2.getAbsolutePath());
            System.out.println("   ### " + f2.getCanonicalPath());
        } catch (Exception e) {
            e.printStackTrace();
        }    
    }
}
