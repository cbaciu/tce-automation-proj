package TCE;

import org.junit.Test;

public class OpenPageTest extends BaseClass{
	
	@Test
	public void test1_OpenPage() throws InterruptedException {
    	System.out.println("   ### Start Test :###" + getName());

		driver.get("http://www.endava.com");
		for(int i=1;i<10;i++){
			System.out.println(driver.getTitle());
			Thread.sleep(1000);
		}
		
    	System.out.println("   ### End Test :###" + getName());		

	}
	
	@Test
	public void test2_OpenPage() throws InterruptedException {
		System.out.println("   ### Start Test :###" + getName());
		
		driver.get("http://www.orange.ro");
		for(int i=1;i<10;i++){
			System.out.println(driver.getTitle());
			Thread.sleep(1000);
		}
		
		System.out.println("   ### End Test :###" + getName());
	}
	
	
	public void BeforeEach(){
		String browserProperty = System.getProperty("browserJenkins");
		System.out.println("   ### The value of browserJenkins got from Jenkins =  " + browserProperty + " #");

		if (browserProperty == null) {
			setBrowsername("firefox");//firefox, chrome, internet explorer
			browserProperty = "firefox";
		} else {
			setBrowsername(browserProperty);
		}
		
		System.out.println("   ### The new value set for browserProperty =  " + browserProperty + " #");
		//set system property - browserJenkinsLocal - to be used later on the driver
		System.setProperty("browserJenkinsLocal", "grid");

		//Variable for storing the value get from Jenkins, or set with chrome by default
		bt = browserProperty;
		setBaseURL("http://www.google.ro");
	}
	

}
